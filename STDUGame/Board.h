#pragma once

#define BOARD_ROW 20
#define BOARD_COLUMN 20

#define PLAYER_1 0
#define PLAYER_2 1
#define MAX_PLAYERS 2

#define MAX_CHAR 100

//The game has two possible states: PLAY (Playing) or EXIT (Exit from the game)
enum GameState { PLAY, EXIT };

// The players needs the x and y position 
typedef struct {
	int x;
	int y;
}PlayerPosition;

//The boardGame struct allows to store all the data about the game
typedef struct {
	char board[BOARD_COLUMN][BOARD_ROW];
	GameState gameState;
	int players[MAX_PLAYERS];
		PlayerPosition player1Position;
		PlayerPosition player2Position;
		int currentPlayer;
}boardGame;




//Initialize the board
void initializeBoard(boardGame * game);
//Update the current player's position
void updatePlayerPosition(boardGame *game, char move);
//Execute the game logic :
void executeGameLogic(boardGame * game);
//Draw the winner based on the current score
void drawBoard(boardGame *game);
//Print the board on the screen
void drawWinner(boardGame * game);


