#pragma once

#include <cstdlib>
#include <stdio.h>
#include "Board.h"

// Initialize the board
void initializeBoard(boardGame* game) {
	int charCount = 0;	// Counter for the initalize points in the board
	bool cellPoint = 0; // Bool for determine if the cell have a point or is void

	game->gameState = PLAY;
	game->currentPlayer = PLAYER_1;
	game->players[0] = PLAYER_1;
	game->players[1] = PLAYER_2;

	//Initialize the board
	for (int i = 0; i < BOARD_COLUMN; i++) { // 0 - 19
		for (int j = 0; j < BOARD_ROW; j++) { // 0 - 19
			// draw wall
			if (i == 0 || i == BOARD_ROW-1) {	// When the board is top or bottom wall
				game->board[i][j] = '_';
			} else if (j == 0 || j == BOARD_ROW-1) { 	// When the board is right or left wall
				game->board[i][j] = '|';
			} else {
				cellPoint = (rand() %4 == 0) ? true: false; // 25% to display a 0, 100 points to 400 cells
				if (cellPoint) {
					game->board[i][j] = '#';
				} else {
					game->board[i][j] = '.';
				}
			}
		}
	}
	// Randomly generate the players position
	game->player1Position.x = rand() % 19 + 1;
	game->player1Position.y = rand() % 19 + 1;
	game->player2Position.x = rand() % 19 + 1;
	game->player2Position.y = rand() % 19 + 1;
	// first we must have to check if the position is correct
	// add the players into board
	game->board[game->player1Position.x][game->player1Position.y] = '1';// player 1
	game->board[game->player2Position.x][game->player2Position.y] = '2';// player 1
}

//Update the current player's position
void updatePlayerPosition(boardGame *game, char move) {

	// I don't like this, but as we have it's the only way.
	PlayerPosition* currentP;
	if (game->currentPlayer == game->players[0]) {
	} else {

	}

	switch (move) {
	case 'w': break; // J--
	case 'a': break; // I--
	case 's': break; // J++
	case 'd': break; // I++

	}
}

//Execute the game logic :
void executeGameLogic(boardGame * game) {
	// WHAT ARE THOSE!!!!!!
	//aaaaa plus the points, no?
	
	
	// Change the current player
	if (game->currentPlayer == game->players[1]) {
		game->currentPlayer = game->players[0];
	} else {
		game->currentPlayer = game->players[1]; 
	}
}

//Print the board on the screen
void drawBoard(boardGame *game) {
	for (int i = 0; i < BOARD_COLUMN; i++) {
		for (int j = 0; j < BOARD_ROW; j++) {
			printf(" %c ", game->board[i][j]);
		}
		printf("\n");
	}
}

//Draw the winner based on the current score
void drawWinner(boardGame * game) {
	system("CLS");
}
