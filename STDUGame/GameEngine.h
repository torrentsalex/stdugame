#pragma once

#include "Board.h"


//Start the game's execution
void run();
//Initialize the game's components
void initialize(boardGame * game);
//Game's execution : Gets input events, processes game
//logic and draws the game's output on the screen
void gameLoop(boardGame * game);
//Get and execute the player's events
void eventHandler(boardGame * game);
//Execute the game's physics/logic
void doPhysics(boardGame * game);
//Draw the game's objects on the screen
void renderGame(boardGame * game);
